import Vue from 'vue'

declare module '*.vue' {
  export default Vue
}

import VueRouter from 'vue-router'
declare module 'vue/types/vue' {
  interface VueConstructor {
    $router: any
  }

  interface Vue {
    $router: VueRouter
  }
}
