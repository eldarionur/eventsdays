import colors from 'vuetify/es5/util/colors'
import { i18nConfig } from './plugins/i18n'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - eventsdays',
    title: 'Eventsdays',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/vuex-persist', ssr: false },
    { src: '~/plugins/vee-validate.ts', ssr: true },
    { src: '~/plugins/i18n.ts' },
    { src: '~/plugins/filterDates.ts' },
    { src: '~/plugins/filterUpper.ts' },
    { src: '~/plugins/routing-init.ts', mode: 'client', ssr: false }, // only on client side
    { src: '~/plugins/google-maps', ssr: true },
    { src: '~/plugins/notifier.ts' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/dotenv'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/firebase',
    '@nuxtjs/i18n'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/'
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  firebase: {
    config: {
      apiKey: process.env.VUE_APP_FIREBASE_API_KEY,
      authDomain: process.env.VUE_APP_FIREBASE_DOMAIN,
      projectId: process.env.VUE_APP_FIREBASE_PROJECT_ID,
      storageBucket: process.env.VUE_APP_FIREBASE_STORAGE_BUCKET,
      messagingSenderId: process.env.VUE_APP_FIREBASE_MESSAGING_SENDER_ID,
      appId: process.env.VUE_APP_FIREBASE_APP_ID,
      databaseUrl: process.env.VUE_APP_FIREBASE_DATABASE_URL
    },

    services: {
      auth: {
        persistence: 'local', // default
        initialize: {
          // onAuthStateChangedMutation: 'ON_AUTH_STATE_CHANGED_MUTATION',
          onAuthStateChangedAction: 'auth/onAuthStateChangedAction',
          subscribeManually: false
        }
        // ssr: true // default is false
        // emulatorPort: 9099,
        // emulatorHost: 'http://localhost'
      },
      firestore: true,
      functions: true,
      storage: true,
      database: true,
      messaging: true
    }
  },

  /* module options */
  i18n: i18nConfig,

  // alias: {
  //   types: resolve(__dirname, '@/../..')
  // },

  // Build Configuration: https://go.nuxtjs.dev/config-build

  build: {
    transpile: ['vee-validate', 'vue2-google-maps']
  },

  router: {
    middleware: 'routing'
  },

  //* Fix firestore query.where issue
  //https://firebase.nuxtjs.org/community/faq/#firestore-using-array-contains-or-array-contains-any-does-not-work
  render: {
    bundleRenderer: {
      runInNewContext: false
    }
  },
  loading: {
    color: 'blue',
    height: '5px'
  }
}
