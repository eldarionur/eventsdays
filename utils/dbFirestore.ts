//* Helper to pull collection data and convert to local

import { handleError, to } from './helpers'

export type QueryBuilder = (q: any) => any

export const collectionStream = async <T, TBack>(p: {
  path: string
  queryBuilder?: any
  mapper: (e: TBack) => T
  vueinst: any
}): Promise<T[]> => {
  const { path, queryBuilder, mapper, vueinst } = p
  let query = vueinst.$store.$fire.firestore.collection(path)

  //** Set where clause
  if (queryBuilder) {
    query = queryBuilder(query)
  }
  const [err, snap] = await to(query.get())
  if (err) {
    handleError(vueinst.$store, err)
    return Promise.reject(err)
  }

  //* Process events
  var events: T[] = []
  snap.forEach((doc: any) => {
    var docData = doc.data() as TBack
    events.push(mapper(docData))
  })

  return events
}
