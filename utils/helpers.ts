// import firebase from 'firebase/app'//TODO:firebase
import 'firebase/firestore'
import 'firebase/functions'
import 'firebase/storage'

// import { v4 as uuidv4 } from 'uuid'
import { FormInputTypes } from '@/custom/forms'
import { RootS } from '@/types/'
import { Dispatch } from 'vuex'
// import { CONFIG_FIREBASE } from '@/custom/config'
// import faker from 'faker'
import _ from 'lodash'

export const finderById = (needleId: string) => {
  return ({ id }: { id: string }) => id === needleId
}

export const to = (promise: Promise<Types.TodoFixMe>) => {
  return promise
    .then((val) => [null, val])
    .catch((err) => {
      console.error('Promise error:', err)
      return [err]
    })
}

//* Root context of vuex
export const RTX = { root: true }

export enum ImageFolder {
  users = 'users'
}

type UploadImageP = {
  file: File
  owner: string
  folder: ImageFolder
  fileName?: string
}

//TODO:firebase
export const uploadImage = async (
  p: UploadImageP,
  email: string,
  password: string
) => {
  return to(Promise.resolve(null))
  //   //** Init firestore
  //   //TODO: convert to restful
  //   if (!firebase.apps.length) {
  //     firebase.initializeApp(CONFIG_FIREBASE)
  //     const goAuth = firebase.auth().signInWithEmailAndPassword(email, password)
  //     const [err] = await to(goAuth)
  //     if (err) {
  //       console.error('error uploading image, auth:', err)
  //       return Promise.reject()
  //     }
  //   }
  //   const fireStore = firebase.storage()
  //   //* Save image
  //   const { file, owner, folder, fileName } = p
  //   if (!file) return Promise.reject(['No file'])
  //   //* Create file metadata - add owner's id
  //   const customMetadata = { owner }
  //   //* Compute image path (folder + path)
  //   let imgFullName = fileName
  //   if (!fileName) {
  //     const imgName = uuidv4()
  //     const imgExt = file?.name.split('.').pop()
  //     imgFullName = `${imgName}.${imgExt}`
  //   }
  //   const imgPath = `${folder}/${imgFullName}`
  //   //* Dispatch to save image remotely and await results
  //   const storageRef = fireStore.ref() // Create a root reference
  //   const imgRef = storageRef.child(imgPath)
  //   const uploadProm: Types.TodoFixMe = imgRef.put(file, { customMetadata })
  //   const [errImg, snapshot] = await to(uploadProm)
  //   if (errImg) return Promise.reject(errImg) //? Exn: return
  //   console.log('Uploaded image, snapshot ', snapshot)
  //   //* Compose img url
  //   const img = ``
  //   return [errImg, img]
}

//return a promise that resolves with a File instance
export const urltoFile = (url: string, name: string, type = 'image/*') => {
  return fetch(url)
    .then(function (res) {
      return res.arrayBuffer()
    })
    .then(function (buf) {
      return new File([buf], name, { type })
    })
}

//* Convert resBlob to dataUrl and resolve
export const blobToData = (blob: Blob | File) => {
  return new Promise((resolve) => {
    const reader = new FileReader()
    reader.onloadend = () => resolve(reader.result)
    reader.readAsDataURL(blob)
  })
}

//* Returns obj to trigger update (potential reactivity caveat)
export const updateFormValues = async (
  formFields: Types.Forms,
  edited: Types.RecordAny
) => {
  const formFieldsUpdated = { ...formFields }
  for (const fieldName in formFieldsUpdated) {
    const f = formFieldsUpdated[fieldName]
    f.value = edited[fieldName]
    if (f.inputType === FormInputTypes.image) {
      if (f.value == '') {
        delete f.value
      } else {

        const file = await urltoFile(f.value, 'img.jpg', 'image/jpeg')

        f.value = file
      }
    }

    //* If field is disallowed to be edited - disable
    f.hidden = f.noEdit
  }
  return formFieldsUpdated
}

export const isPositionValid = (pos?: Types.Position | null): boolean => {
  if (!pos) return false
  const lat = pos.lat
  const lng = pos.lng
  return !!(lat && lng && lat > -90 && lat < 90 && lng > -180 && lng < 180)
}

export const getKeyByValue = (
  object: { [key in string]: unknown },
  value: unknown
) => {
  return Object.keys(object).find((key) => object[key] === value)
}

//* Timeout function - intended to be used with async
export const timeout = (ms = 1000) => {
  return new Promise((res) => window.setTimeout(res, ms))
}

/**
 * Returns the index of the last element in the array where predicate is true, and -1
 * otherwise.
 * @param array The source array to search in
 * @param predicate find calls predicate once for each element of the array, in descending
 * order, until it finds one where predicate returns true. If such an element is found,
 * findLastIndex immediately returns that element index. Otherwise, findLastIndex returns -1.
 */
export function findLastIndex<T>(
  array: Array<T>,
  predicate: (value: T, index: number, obj: T[]) => boolean
): number {
  let l = array.length
  while (l--) {
    if (predicate(array[l], l, array)) return l
  }
  return -1
}

export const createComment = () => {
  return {
    id: '',
    text: '',
    createdAt: '',
    parentId: undefined,
    upvotes: 0,
    upvotesUsers: [],
    iUpvote: false,
    iDownvote: false,
    parent: undefined
  }
}

export const handleErr = (err: Types.TodoFixMe) => {
  console.error('Error occured: ', err)
}

//*** SendReq - sending axios requests through dispatch

//* Wrapper, helper to send requests to myxios instance
type SendReqP = {
  url: Types.ReqUrl
  data?: Types.FormDataAny
  options?: { [key in string]: Types.FormDataAny }
  headers?: { [key in string]: Types.FormDataAny }
  onSuccess?: () => void
  hideError?: boolean
}

//* Promise version
export const sendReqProm = (dispatch: Dispatch, payload: SendReqP) => {
  return dispatch(RootS.myxios, payload, RTX)
}
//* Async/await version
export const sendReq = (dispatch: Dispatch, payload: SendReqP) => {
  return to(sendReqProm(dispatch, payload))
}

export const fakeNickname = () => {
  // todo:faker
  return 'TODO:faker'
  // const adj1 = faker.commerce.productAdjective()
  // const adj2 = faker.hacker.adjective()
  // /* eslint-disable */
  // const noun = faker.company.bsNoun()
  // const nickname = _.capitalize(adj1) + _.capitalize(adj2) + _.capitalize(noun)
  // return nickname.replace(/-/g, '').replace(/ /g, '')
}

export const makeTags = (tagsQuery: string | (string | null)[]) => {
  if (typeof tagsQuery === 'string') {
    return [tagsQuery]
  }
  return tagsQuery as string[]
}

const lineToTags = (l: string) => {
  const isValidTag = (tag: string) => {
    const isMinOk = tag.length >= 3
    const isMaxOk = tag.length <= 30
    if (!isMinOk || !isMaxOk) return false
    return true
  }

  const tagsRaw = l.split('#')
  const result: string[] = []
  tagsRaw.forEach((t: string) => {
    const tagSanitized = t.trim()
    if (!isValidTag(tagSanitized)) return
    result.push(tagSanitized)
  })
  return result
}

// TODO: extract rules into laravel format
export const msgToTagsText = (msg: string) => {
  //* Define resulting data
  let tags: string[] = []
  let text = ''
  //* Process each line (sep newline), separate lines starting with # from text
  const tagLines = []
  const msgLines = msg.split(/\r?\n/)
  const SEPARATOR = '\r\n'

  //* Handle tags at the beginning
  let textStartIndex = 0
  let textEndIndex = 0
  for (let i = 0; i < msgLines.length; i++) {
    const l = msgLines[i]
    if (l.trim().startsWith('#')) {
      tagLines.push(l.trim())
    } else if (l.trim().length > 1) {
      textStartIndex = i
      break
    }
  }

  //* Handle tags at the end
  const msgEndLines = msgLines.slice(textStartIndex)
  const tagLinesEnding = []

  for (let i = msgEndLines.length - 1; i >= 0; i--) {
    const l = msgEndLines[i]
    if (l.trim().startsWith('#')) {
      tagLinesEnding.push(l.trim())
    } else if (l.trim().length > 1) {
      textEndIndex = textStartIndex + i
      break
    }
  }

  text = msgLines.slice(textStartIndex, textEndIndex + 1).join(SEPARATOR)

  //* Each line starting with # - further split into tags and sanitation
  tagLines.forEach((l: string) => (tags = [...tags, ...lineToTags(l)]))
  tagLinesEnding
    .reverse()
    .forEach((l: string) => (tags = [...tags, ...lineToTags(l)]))
  return {
    tags: _.uniq(tags).map((t: string) => t.toUpperCase()),
    text
  }
}

export const parseText = (text: string, limit = 100) => {
  if (text.length > limit)
    for (let i = limit; i > 0; i--) {
      if (
        text.charAt(i) === ' ' &&
        (text.charAt(i - 1) != ',' ||
          text.charAt(i - 1) != '.' ||
          text.charAt(i - 1) != ';')
      ) {
        return text.substring(0, i) + '...'
      }
    }
  else return text
}

export const mapEvent = (doc: Events.EventBackend) => {
  const event: Events.Event = {
    id: doc.id,
    createdAt: doc.createdAt,
    date: doc.date,
    title: doc.title,
    img: doc.img,
    text: doc.text,
    location: doc.location,
    organizerId: doc.organizerId,
    organizerName: doc.organizerName ?? '',
    users: doc.users,
    positionLat: doc.positionLat,
    positionLng: doc.positionLng,
    replies: doc.replies ?? 0,
    place: doc.place ?? '',
    attendees: []
  }
  return event

}

//* Chunk array into given length
export function chunks(inputArray: any[], perChunk: number) {
  return inputArray.reduce((resultArray, item, index) => {
    const chunkIndex = Math.floor(index / perChunk)
    if (!resultArray[chunkIndex]) {
      resultArray[chunkIndex] = [] // start a new chunk
    }
    resultArray[chunkIndex].push(item)
    return resultArray
  }, [])
}

//TODO: fix tsconfig
export function routerpush(vueinst: any, val: any) {
  // @ts-ignore
  vueinst.$router.push(val)
}

//TODO: fix tsconfig
export function showSnack(vueinst: any, msg: string) {
  // @ts-ignore
  vueinst.$notifier.showMessage({
    content: msg,
    color: 'info'
  })
}

//TODO: fix tsconfig
//* Get translation
export function txt(vueinst: any, name: string): string {
  // @ts-ignore
  return vueinst.$txt[name]()
}

export const HOUR = 1000 * 60 * 60

export function isEventCompleted(date: number, t: number = HOUR * 1.5) {
  const endTime = Date.now() - t

  return date < endTime
}

export function handleError(vueinst: any, errorMsg: any) {
  console.log('ERROR:', errorMsg)
  showSnack(vueinst, 'Error, please check your network and try again')

  return
}

export function makeEventImageName(userId: string, eventId: string) {
  return userId + '_' + eventId
}

export function extractFileName(str: string) {
  return str.split(/(\\|\/)/g).pop() || ''
}

export function extractFileNameNoExt(str: string) {
  const fpath = str.replace(/\\/g, '/')
  return fpath.substring(fpath.lastIndexOf('/') + 1, fpath.lastIndexOf('.'))
}
