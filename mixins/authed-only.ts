import { Vue, Component } from 'vue-property-decorator'
import { namespace } from 'vuex-class'
import { routerpush } from '~/utils/helpers'

const auth = namespace('auth')

//*===🔷===🔷===🔷===🔷===🔷===🔷===🔷===🔷===🔷===🔷===🔷===🔷Primary

@Component
export default class AuthedOnly extends Vue {
  @auth.Getter isLogged!: boolean

  mounted() {
    if (this.isLogged) {
      routerpush(this, '/')

      // return (this as any).$nuxt.error({
      //   statusCode: 404,
      //   message: 'err message'
      // })
    }

    // do something that should be done at Client side, not Server Side.
  }
}
