import { Vue, Component, Prop, Emit } from 'vue-property-decorator'
import { ATTEND_STATE } from '~/store/events'


//*===🔷===🔷===🔷===🔷===🔷===🔷===🔷===🔷===🔷===🔷===🔷===🔷Primary

@Component
export default class EventAttendance extends Vue {

  //*===👜===👜===👜===👜===👜===👜===👜===👜===👜===👜===👜===👜Props

  @Prop() event: Events.Event
  @Prop() user: Auth.User
  @Prop() loading: boolean

  //*===🐍===🐍===🐍===🐍===🐍===🐍===🐍===🐍===🐍===🐍===🐍===🐍Emits

  @Emit() change(v: ATTEND_STATE) {
    return v
  }

  //*===🍏===🍏===🍏===🍏===🍏===🍏===🍏===🍏===🍏===🍏===🍏===🍏Computed

  get countAttend() {
    if (!this.event.users) return 0
    const users = this.event.users
    return users.reduce(
      (acc, eu) => acc + (eu.v == ATTEND_STATE.YES ? 1 : 0),
      0
    )
  }

  get countMaybe() {
    if (!this.event.users) return 0
    const users = this.event.users
    return users.reduce(
      (acc, eu) => acc + (eu.v == ATTEND_STATE.MAYBE ? 1 : 0),
      0
    )
  }

  get eventUser() {
    if (!this.user) return null
    const finder = (eu: Events.EventUser) => eu.userId == this.user!.id
    return this.event.users?.find(finder)
  }

  get isNotGoing() {
    return this.eventUser?.v == ATTEND_STATE.NO
  }
  get isAttending() {
    return this.eventUser?.v == ATTEND_STATE.YES
  }
  get isMaybe() {
    return this.eventUser?.v == ATTEND_STATE.MAYBE
  }

  //*===🌊===🌊===🌊===🌊===🌊===🌊===🌊===🌊===🌊===🌊===🌊===🌊Methods

  onAttend() {
    if (this.user?.id == this.event.organizerId) return
    const newState = this.isAttending ? ATTEND_STATE.EMPTY : ATTEND_STATE.YES
    this.change(newState)
  }

  onMaybe() {
    if (this.user?.id == this.event.organizerId) return

    const newState = this.isMaybe ? ATTEND_STATE.EMPTY : ATTEND_STATE.MAYBE
    this.change(newState)
  }

  onCancel() {
    this.change(ATTEND_STATE.NO)
  }
}
