import moment from 'moment/moment'
import { Events, Languages } from '@/types'
import { EventBus } from '@/utils/eventBus'
import { createModule, mutation, action } from 'vuex-class-component'

const VuexModule = createModule({
  namespaced: 'language',
  strict: false,
  target: 'nuxt'
})

export default class Language extends VuexModule {
  // state
  private _lang = Languages.en

  // getters
  get lang(): Languages {
    //TODO: pull user, for now mock only
    // var user = null
    // if (user && user.language) {
    //   return user.language
    // }

    return this._lang
    // return i18n.locale as Auth.Language //TODO:i18n
  }

  // mutations

  @mutation
  public _setLang(lang: Auth.Language = Languages.en) {
    this._lang = lang
  }

  // actions
  @action
  public async setLang(langRaw: Auth.Language): Promise<void> {
    // cast code to lowercase, since i18n and moment are using lowercase code
    const lang = langRaw.toLowerCase() as Auth.Languages
    EventBus.$emit(Events.ShowLoader)
    this._setLang(lang)
    // i18n.locale = lang //TODO:i18n
    moment.locale(lang)
    return Promise.resolve()
  }
}
