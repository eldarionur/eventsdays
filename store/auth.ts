import { createModule, mutation, action } from 'vuex-class-component'
import { Routes } from '~/router'
import {
  extractFileName,
  extractFileNameNoExt,
  timeout,
  to
} from '~/utils/helpers'
import { getAuth, signInWithPopup, GoogleAuthProvider } from 'firebase/auth'

export const AUTH_NAMESPACE = 'auth'

const VuexModule = createModule({
  namespaced: 'auth',
  strict: false,
  target: 'nuxt'
})

export default class Auth extends VuexModule {
  // state
  private _user: Auth.User | null = null
  private _profile: Auth.ProfilePublic | null = null

  //*===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙Getters

  get user(): Auth.User {
    return this._user
  }

  get profile(): Auth.ProfilePublic | null {
    return this._profile
  }

  get isLogged(): boolean {
    return this._user != null
  }

  //*===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧Mutations

  @mutation
  public _setUser(user: Auth.User | null = null) {
    this._user = user
  }

  @mutation
  public _setProfile(profile: Auth.ProfilePublic | null = null) {
    this._profile = profile
  }

  //*===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖Actions

  @action
  public async setUser(user: Auth.User): Promise<void> {
    this._setUser(user)
    return Promise.resolve()
  }

  //TODO
  @action
  public async signin({ email, password }: Auth.SigninP): Promise<void> {
    const [err, __] = await to(
      this.$store.$fire.auth.signInWithEmailAndPassword(email, password)
    )

    //? Exn: firebase error
    if (err) return Promise.reject(err)
    return Promise.resolve()

    var user: Auth.User = {
      id: 'uuid-johndoe',
      displayName: 'johndoe',
      email: email,
      phone: 'phoneNumber',
      img: ''
      // nickname: 'johndoe',
      // language: Languages.en,
      // name: 'John',
      // surname: 'Doe'
    }
    await timeout(2000) //* Wait
    this._setUser(user)
    return Promise.resolve()
  }

  @action
  public async signinGoogle(): Promise<void> {
    const auth = getAuth()
    const provider = new GoogleAuthProvider()
    const [err, res] = await to(signInWithPopup(auth, provider))
    if (err) {
      // Handle Errors here.
      // const errorCode = err.code
      const errorMessage = err.message
      // The email of the user's account used.
      // const email = err.email
      // The AuthCredential type that was used.
      // const credential = GoogleAuthProvider.credentialFromError(err)
      // ...
      console.log('errorMessage:', errorMessage)
      return Promise.reject(err)
    }
    // This gives you a Google Access Token. You can use it to access the Google API.
    const credential = GoogleAuthProvider.credentialFromResult(res)
    if (!credential) return Promise.reject()
    const token = credential.accessToken
    // The signed-in user info.
    const user = res.user

    if (!user || !token) {
      return Promise.reject()
    }
    // ...
    return Promise.resolve(user)
  }

  //TODO
  @action
  public async signup({ email, password }: Auth.SignupP): Promise<void> {
    const [err, __] = await to(
      this.$store.$fire.auth.createUserWithEmailAndPassword(email, password)
    )

    //TODO: if bug with setting nickname
    // this.$store.$router.push({ path: Routes.account.children.details.path })

    //? Exn: firebase error
    if (err) return Promise.reject(err)
    return Promise.resolve()
  }

  @action
  public async updateProfile(data: Auth.UpdateProfileP): Promise<void> {
    //* Pull user id
    const profileOld = this.profile
    const user = this.user
    if (!user) return Promise.reject()

    //* Compose profile object
    const profile: Auth.ProfilePublic = {
      id: user.id,
      displayName: data.displayName,
      img: (data.img ? data.img : profileOld?.img) ?? '',
      isModerated: false
    }

    const apiPath = `/profiles_public/${profile.id}`
    const reference = this.$store.$fire.firestore.doc(apiPath)

    const [err, __] = await to(reference.set(profile))
    if (err) return Promise.reject(err)

    // also changed user name on events
    this.changeNameOnEvents(profile)

    // also changed user name on comments 
    this.changeNameOnComments(profile)

    this._setProfile(profile)
    return Promise.resolve()
  }

  @action
  public async changeNameOnEvents(profile: Auth.ProfilePublic) {
    if (profile == null) return

    //TODO - update events with another user name
    await this.$store.dispatch('events/fetchEventsById', profile.id)

    const allEvents = await this.$store.getters['events/allevents'];

    allEvents.forEach(function (item: Events.EventBackend) {
      item.organizerName = profile.displayName
    }
    );

    const db = this.$store.$fire.firestore

    // Get a new write batch
    var batch = db.batch();

    allEvents.forEach((element: Events.EventBackend) => {
      var ref = db.doc(`/events/${element.id}`)
      batch.update(ref, { organizerName: element.organizerName });
    });

    // Commit the batch
    batch.commit()
  }

  @action
  public async changeNameOnComments(profile: Auth.ProfilePublic) {
    if (profile == null) return

    //TODO - update events with another user name
    await this.$store.dispatch('comments/fetchCommentsByUserId', profile.id)

    const commentsByUserId = await this.$store.getters['comments/commentsByUserId'];

    commentsByUserId.forEach(function (item: Comments.CommentV2) {
      item.userName = profile.displayName
    });

    const db = this.$store.$fire.firestore

    // Get a new write batch
    var batch = db.batch();

    commentsByUserId.forEach((element: Comments.CommentV2) => {
      var ref = db.doc(`/comments/${element.id}`)
      batch.update(ref, { userName: element.userName });
    });

    // Commit the batch
    batch.commit()
  }


  //todo
  //* Clear store, firebase logout
  @action
  public async signout({ nonauthed }: Auth.SignoutP): Promise<void> {
    if (nonauthed) {
      this._setUser()
      this._setProfile()
      return Promise.resolve()
    }
    await this.$store.$fire.auth.signOut()
    this._setUser()
    this._setProfile()
    return Promise.resolve()
  }

  @action
  public async reset(): Promise<void> {
    this._setUser()
    return Promise.resolve()
  }

  @action
  public async onAuthStateChangedAction({
    authUser,
    claims
  }: any): Promise<void> {
    //? if not logged and not authed - resolve
    if (authUser == null) {
      if (!this.isLogged) {
        return Promise.resolve()
      } else {
        //? If logged and not authed - signout
        return this.signout({ nonauthed: true })
      }
    }

    //? Exn: user is authed and arleady set
    if (this.isLogged) return Promise.resolve()

    //* Cast remote to local user
    const authUserLocal: Auth.User = {
      id: authUser.uid,
      displayName: authUser.displayName,
      email: authUser.email,
      phone: authUser.phoneNumber,
      img: authUser.photoURL,
      isVerified: authUser.isVerified
    }

    //* Update store with user details
    //* Timeout required mb coz action is triggered first
    setTimeout(() => {
      this.setUser(authUserLocal)
      this.checkProfile()
    }, 30)
    return Promise.resolve()
  }

  //* If user is logged, but no profile - redirect
  @action
  public async checkProfile(): Promise<void> {
    //* check if user is logged
    if (!this.isLogged) return Promise.resolve()
    if (this.profile != null) return Promise.resolve()
    if (this.user == null) return Promise.reject()

    //* attempt to pull profile
    const apiPath = `/profiles_public/${this.user.id}`
    const doc = await this.$store.$fire.firestore.doc(apiPath).get()

    var profile = (doc.data() as Auth.ProfilePublic) || undefined

    //? Exn: Profile is not set, redirect to create
    if (!profile) {
      this.$store.$router.push({ path: Routes.account.children.details.path })
      return Promise.resolve()
    }

    //* Save profile if set
    this._setProfile(profile)
    return Promise.resolve()
  }

  @action
  public async recoverPassword({
    email
  }: Auth.RecoverPasswordP): Promise<void> {
    const [err, __] = await to(
      this.$store.$fire.auth.sendPasswordResetEmail(email)
    )

    //? Exn: firebase error
    if (err) return Promise.reject(err)
    return Promise.resolve()
  }

  //* If img is updated - delete old img
  @action
  async uploadImg(p: Auth.UploadImgP) {
    const { file, name } = p
    if (!file) return Promise.reject(['No file'])

    const fireStore = this.$store.$fire.storage

    const user = this.$store.getters['auth/user']
    const owner = user.id

    const folder = 'users' //TODO: move outside
    let imgName = name
    imgName = owner //TODO:

    //* Save image

    //* Compute image path (folder + path)
    let imgFullName = ''
    const imgExt = file?.name.split('.').pop()
    imgFullName = `${imgName}.${imgExt}`
    let imgPath = `${folder}/${imgFullName}`

    //* Create file metadata - add owner's id
    const customMetadata = { owner }

    //* Dispatch to save image remotely and await results
    const storageRef = fireStore.ref() // Create a root reference

    console.log('imgPath', imgPath)
    //* If updating image, remove previous one

    if (p.oldImgPath) {
      //TODO: filename extraction logic might be faulty
      //TODO: Since it's based on public url, not an actual ref

      //* Extract file path from old filename
      const oldImgFullName: string = extractFileName(p.oldImgPath)
      imgPath = `${folder}/${oldImgFullName}`

      //* Delete old img
      const imgRefOld = storageRef.child(imgPath)
      const [errDelete, __] = await to(imgRefOld.delete())

      if (errDelete) {
        return Promise.reject(errDelete)
      }

      //* Make new path (for the sake of cdn/cache invalidation)
      const oldImgName = extractFileNameNoExt(oldImgFullName)
      imgFullName = `${oldImgName}_.${imgExt}`
      imgPath = `${folder}/${imgFullName}`
    }
    const imgRef = storageRef.child(imgPath)

    const uploadProm: Types.TodoFixMe = imgRef.put(file, { customMetadata })

    const [errImg, __] = await to(uploadProm)

    if (errImg) return Promise.reject(errImg) //? Exn: return

    // console.log('Uploaded image, snapshot ', snapshot)

    //* Compose img url
    const imgLink = `https://storage.googleapis.com/deals-4444.appspot.com/${imgPath}`
    return Promise.resolve(imgLink)
  }
}
