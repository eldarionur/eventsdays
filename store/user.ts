import { createModule, mutation, action } from 'vuex-class-component'


export const UsersNamespace = 'user'
const VuexModule = createModule({
  namespaced: UsersNamespace,
  strict: false,
  target: 'nuxt'
})

export default class Users extends VuexModule {

  private _user: Auth.ProfilePublic | null = null

  @mutation
  private _setUser(user: Auth.ProfilePublic | null = null) {
    this._user = user
  }

  @action
  public async fetchUser(userid: any): Promise<void> {

    const apiPath = `/profiles_public/${userid}`

    const doc = await this.$store.$fire.firestore.doc(apiPath).get()

    var user = (doc.data() as Auth.ProfilePublic) || undefined

    this._setUser(user);
  }

  // getters  
  get user(): Auth.ProfilePublic {
    return this._user
  }

}
