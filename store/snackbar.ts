import { createModule, mutation, action } from 'vuex-class-component'

const VuexModule = createModule({
  namespaced: 'snackbarNamespace',
  strict: false,
  target: 'nuxt'
})

export default class Snackbar extends VuexModule {
  // state
  private _content = ''
  private _color = ''

  // getters
  get content(): string {
    return this._content
  }
  get color(): string {
    return this._color
  }

  @mutation
  public _showMessage(payload: any) {
    this._content = payload.content
    this._color = payload.color
  }

  // actions
  @action
  public async showSnackbar(payload: any): Promise<void> {
    this._showMessage(payload)
  }
}
