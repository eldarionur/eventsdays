import { createModule, mutation, action } from 'vuex-class-component'
import {
  chunks,
  extractFileName,
  extractFileNameNoExt,
  finderById,
  HOUR,
  makeEventImageName,
  mapEvent,
  to
} from '~/utils/helpers'
import Vue from 'vue'
import _ from 'lodash'
import { EVENT_COMPLETED_DEADLINE } from '~/custom/config'
import { collectionStream } from '~/utils/dbFirestore'

const VuexModule = createModule({
  namespaced: 'events',
  strict: false,
  target: 'nuxt'
})

type Event = Events.Event
type EventBack = Events.EventBackend
type EventUser = Events.EventUser
type QueryBuilder = (q: any) => any
export const EVENTS_NAMESPACE = 'events'

export enum ATTEND_STATE {
  EMPTY = 0,
  NO = 1,
  MAYBE = 2,
  YES = 3
}

const mapEventUser = (data: any): EventUser => {
  return {
    userId: data.userId ?? '',
    eventId: data.eventId ?? '',
    v: data.v ?? 0
  }
}

export default class Events extends VuexModule {
  // state
  private _events: Events.Event[] = []
  private _event: Events.Event | null = null
  private _allevents: Events.Event[] = []


  //*===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙Getters

  get events(): Events.Event[] {
    return this._events
  }

  get event(): Events.Event | null {
    return this._event
  }

  get allevents(): Events.Event[] {
    return this._allevents
  }
  //*===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧Mutations

  @mutation
  public _setEvents(events: Events.Event[] = []) {
    //* Sort events and set
    this._events = events
  }

  @mutation
  public _setAllEvents(events: Events.Event[] = []) {
    //* Sort events and set
    this._allevents = events
  }

  @mutation
  public _addEvent(event: Events.Event) {
    this._events = [event, ...this._events]
  }

  @mutation
  public _setEvent(event: Events.Event) {
    this._event = event
  }

  @mutation
  public _updateEvent(event: Events.Event) {
    // Update records array
    const findById = finderById(event.id)
    const index = this._events.findIndex(findById)

    //? Exn: element not found
    if (index === -1) return

    //* Update the element
    Vue.set(this._events, index, event)
  }

  //*===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖Actions

  @action
  async nuxtServerInit() {
    // await timeout(5000)
    // const reference = this.$store.$fire.firestore
    //   .collection('/events')
    //   .onSnapshot((data) => {
    //     // console.log('onsnap', data)
    //   })
    // console.log('fetchevents:', reference)
    // commit('init_data_for_firmenistorie2', 123)
  }

  //* Fetch and return userEvent by list of id's
  //* Chunking is due to restriction of firebase is 'where..in' for 10 items
  @action
  public async fetchUsers(eventIds: string[]): Promise<Events.EventUser[]> {
    var chunkedIds = [...chunks(eventIds, 10)]

    var promises = []
    for (const chunk of chunkedIds) {
      //* If event ids are set, return only events that belong there
      let queryBuilder: QueryBuilder | null = null
      if (eventIds.length > 0) {
        queryBuilder = (q) => q.where('eventId', 'in', chunk)
      }

      type T = EventUser
      const newPromise = collectionStream<T, T>({
        path: '/event_user',
        queryBuilder: queryBuilder,
        mapper: mapEventUser,
        vueinst: this
      }).then((res) => res)
      promises.push(newPromise)
    }

    const [err, res] = await to(Promise.all(promises))
    if (err) return Promise.reject(err)
    //* Merge result
    return res.reduce(
      (acc: EventUser[], chunk: EventUser[]) => [...acc, ...chunk],
      []
    )
  }

  //* Fetch current user's eventUser
  @action
  public async fetchUsersById(userId: string): Promise<Events.EventUser[]> {
    let queryBuilder: QueryBuilder | null = null
    queryBuilder = (q) => q.where('userId', '==', userId)

    type T = EventUser
    const [err, res] = await to(
      collectionStream<T, T>({
        path: '/event_user',
        queryBuilder: queryBuilder,
        mapper: mapEventUser,
        vueinst: this
      })
    )

    if (err) {
      return Promise.reject(res)
    }

    return res
  }

  //* Fetch and return userEvent by list of id's
  @action
  public async fetchEvents(p: {
    eventIds: string[]
    newerThan?: number
    olderThan?: number
    organizerId?: string
  }): Promise<Event[]> {
    console.log('fetchEvent p', p)

    const { eventIds, newerThan, olderThan, organizerId } = p
    //* If event ids are set, return only events that belong there
    let queryBuilder: QueryBuilder | null = null
    queryBuilder = (q) => {
      if (eventIds.length > 0) {
        q = q.where('id', 'in', eventIds)
      }
      if (newerThan) {
        q = q.where('date', '>', newerThan)
      }
      if (olderThan) {
        q = q.where('date', '<', olderThan)
      }
      if (organizerId) {
        q = q.where('organizerId', '==', organizerId)
      }
      if (organizerId && newerThan) {
        q = q.where('organizerId', '==', organizerId).where('date', '>', newerThan)
      }
      return q
    }

    type T = Event
    const events: T[] = await collectionStream<T, EventBack>({
      path: '/events',
      queryBuilder: queryBuilder,
      mapper: mapEvent,
      vueinst: this
    })

    //* Save to store
    return Promise.resolve(events)
  }

  @action
  public async fetch(): Promise<any> {
    //* Show upcoming events only
    const newerThan = +new Date() - HOUR * EVENT_COMPLETED_DEADLINE
    const [__, ___] = await to(this.fetchEventsWithUsers({ newerThan }))
    const sortP = { field: 'date', isAsc: true }
    return this.sort(sortP)
  }

  //* Fetch all events
  @action public async fetchEventsById(p: {
    newerThan?: number
    olderThan?: number
    organizerId: string
  }): Promise<any> {
    const events = await this.fetchEvents({
      eventIds: [],
      organizerId: p.organizerId,
      newerThan: p.newerThan,
      olderThan: p.olderThan
    })

    //* Save to store
    this._setAllEvents(events)
  }

  @action
  public async sort(p: { field: string; isAsc: boolean }): Promise<Event[]> {
    const { field, isAsc } = p
    const order = isAsc ? 'asc' : 'desc'
    const dataSorted = _.orderBy(this.events, field, order)
    this._setEvents([...dataSorted])
    return Promise.resolve(dataSorted)
  }

  @action
  public async fetchEventsWithUsers(p: {
    eventIds?: string[]
    newerThan?: number
    olderThan?: number
    organizerId?: string
  }): Promise<void> {
    //* Get snap for events collection
    const events = await this.fetchEvents({
      eventIds: p.eventIds ? p.eventIds : [],
      newerThan: p.newerThan,
      olderThan: p.olderThan,
      organizerId: p.organizerId
    })

    //* Save to store
    this._setEvents(events)

    //* Fetch all user attendance votes for the events
    const eventIds = events.map((e) => e.id)
    const [err, eventUsers] = await to(this.fetchUsers(eventIds))
    if (err) return Promise.resolve() //? exn

    // *Process event users
    eventUsers.forEach((eventUser: Events.EventUser) => {
      const findById = finderById(eventUser.eventId)
      const event = this.events.find(findById)
      if (!event) return //?exn
      if (!event.users) {
        event.users = []
      }
      event.users.push(eventUser)
    })
    this._setEvents(this.events)

    return Promise.resolve()
  }

  @action
  public async fetchMyEvents(p: { isPast: boolean }): Promise<void> {
    //* Fetch all user attendance votes for the events
    const user = this.$store.getters['auth/user']

    //* Clear current events list
    this._setEvents([])

    if (user == null) return Promise.resolve()

    const [errUsers, eventUsersAll] = await to(this.fetchUsersById(user.id))
    if (errUsers) return Promise.reject(errUsers)

    //* Get snap for events collection
    //* Get eventUsers that have status value not representing "NOT_GOING"
    const filterAttending = (e: EventUser) => {
      return e.v == ATTEND_STATE.MAYBE || e.v == ATTEND_STATE.YES
    }
    const eventUsers = eventUsersAll.filter(filterAttending)

    const eventIds = eventUsers.map((e: EventUser) => e.eventId)

    //* Before fetching events, apply date modifier
    const fetchP: {
      eventIds: string[]
      olderThan?: number
      newerThan?: number
    } = {
      eventIds
    }

    if (p.isPast) {
      fetchP.olderThan = +new Date()
    } else {
      fetchP.newerThan = +new Date()
    }

    if (fetchP.eventIds.length > 0) {
      return this.fetchEventsWithUsers(fetchP)
    }
    else {
      this._setEvents()
    }

  }

  @action
  public async fetchOrganized(): Promise<void> {
    //* Pull user id
    const user = this.$store.getters['auth/user']
    const organizerId = user.id
    //* Pull events where the user is organizer
    const fetchP = { organizerId }
    return this.fetchEventsWithUsers(fetchP)
  }

  @action
  public async fetchEvent(eventId: string): Promise<void> {
    const doc = await this.$store.$fire.firestore
      .doc(`/events/${eventId}`)
      .get()

    var docData = doc.data() as Events.EventBackend
    var event: Events.Event = mapEvent(docData)
    //* Save to store

    const [err, eventUsers] = await to(this.fetchUsers([event.id]))
    if (err) return Promise.resolve() //? exn

    // *Process event users
    event.users = eventUsers
    this._setEvent(event)

    return Promise.resolve()
  }

  @action
  public async create(data: Events.CreateP): Promise<void> {
    console.log('Store:Creating event...', data)

    //* Pull user id
    const user = this.$store.getters['auth/profile']
    const organizerId = user.id
    const organizerName = user.displayName

    //* Compose event object
    const event: Events.EventBackend = {
      id: data.id,
      createdAt: Date.now().valueOf(),
      date: data.date,
      title: data.title,
      text: data.text,
      organizerId: organizerId,
      organizerName: organizerName,
      location: 'New York', //TODO:
      attendees: [], //TODO
      users: [], //TODO
      img: data.img ?? '',
      positionLat: data.positionLat,
      positionLng: data.positionLng,
      place: data.place ?? ''
    }

    const reference = this.$store.$fire.firestore.doc(`/events/${event.id}`)

    const [__, ___] = await to(reference.set(event))

    // this.$store.

    this._addEvent(mapEvent(event))

    return Promise.resolve()
  }

  @action
  public async update(data: Events.CreateP): Promise<void> {
    const reference = this.$store.$fire.firestore.doc(`/events/${data.id}`)
    const [__, ___] = await to(reference.update(data))
    await this.fetchEvent(data.id)
    return Promise.resolve()
  }

  //* If img is updated - delete old img
  @action
  async uploadImg(p: Events.UploadImgP) {
    const { file, eventId } = p
    if (!file) return Promise.reject(['No file'])

    const fireStore = this.$store.$fire.storage

    const user = this.$store.getters['auth/user']
    const owner = user.id
    const folder = 'events'

    //* Save image

    //* Compute image path (folder + path)
    let imgFullName = ''
    const imgName = makeEventImageName(user.id, eventId)
    const imgExt = file?.name.split('.').pop()
    imgFullName = `${imgName}.${imgExt}`
    let imgPath = `${folder}/${imgFullName}`

    //* Create file metadata - add owner's id
    const customMetadata = { owner }

    //* Dispatch to save image remotely and await results
    const storageRef = fireStore.ref() // Create a root reference

    //* If updating image, remove previous one

    if (p.oldImgPath) {
      //TODO: filename extraction logic might be faulty
      //TODO: Since it's based on public url, not an actual ref

      //* Extract file path from old filename
      const oldImgFullName: string = extractFileName(p.oldImgPath)
      imgPath = `${folder}/${oldImgFullName}`

      //* Delete old img
      const imgRefOld = storageRef.child(imgPath)
      const [errDelete, __] = await to(imgRefOld.delete())

      if (errDelete) {
        return Promise.reject(errDelete)
      }

      //* Make new path (for the sake of cdn/cache invalidation)
      const oldImgName = extractFileNameNoExt(oldImgFullName)
      imgFullName = `${oldImgName}_.${imgExt}`
      imgPath = `${folder}/${imgFullName}`
    }


    const imgRef = storageRef.child(imgPath)

    const uploadProm: Types.TodoFixMe = imgRef.put(file, { customMetadata })
    const [errImg, __] = await to(uploadProm)
    if (errImg) return Promise.reject(errImg) //? Exn: return
    // console.log('Uploaded image, snapshot ', snapshot)

    //* Compose img url
    const imgLink = `https://storage.googleapis.com/deals-4444.appspot.com/${imgPath}`
    return Promise.resolve(imgLink)
  }

  @action
  public async attend(p: Events.AttendP): Promise<void> {
    const { eventId, v } = p
    //* Pull user id
    const userId = this.$store.getters['auth/user'].id

    const euId = `${eventId}x${userId}`
    const ref = this.$store.$fire.firestore.doc('event_user/' + euId)
    const [err, __] = await to(ref.set({ v, eventId, userId }))
    if (err) return Promise.reject(err)

    const findById = finderById(eventId)
    //* Pull event, either single or from list
    let event = this.event
    if (!event) {
      const eventIndex = this.events.findIndex(findById)
      if (eventIndex < 0) return
      event = this.events[eventIndex]
    }

    if (!event) return Promise.reject()
    if (!event.users) {
      event.users = []
    }
    //* Get specific user
    const finderUser = (eu: Events.EventUser) => eu.userId == userId
    var index = event.users.findIndex(finderUser)
    const eu = { eventId, userId, v }
    //If no eu item -push
    if (index < 0) {
      event.users.push(eu)
    } else {
      event.users[index] = eu
    }

    //! Reactivity issue fix
    const eventClone = _.cloneDeep(event)
    //* If got event from `events` state
    const eventIndex = this.events.findIndex(findById)
    if (eventIndex >= 0) {
      this.events[eventIndex] = eventClone
      this._setEvents([...this.events])
    }

    this._setEvent(eventClone)

    return Promise.resolve()
  }

  @action
  public async updateReplies(p: {
    eventId: string
    replies: number
  }): Promise<void> {
    const { eventId, replies } = p
    //! Reactivity issue fix
    //* If got event from `events` state
    const findById = finderById(eventId)
    const eventIndex = this.events.findIndex(findById)
    if (eventIndex >= 0) {
      const eventClone = this.events[eventIndex]
      eventClone.replies = replies
      this.events[eventIndex] = eventClone
      this._setEvents([...this.events])
    }

    if (this.event && this.event.id == eventId) {
      const eventClone = this.event
      eventClone.replies = replies
      this._setEvent(eventClone)
    }

    return Promise.resolve()
  }
}
