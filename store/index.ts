import Vue from 'vue'
import Vuex from 'vuex'
import { createProxy, extractVuexModule } from 'vuex-class-component'
import Auth from './auth'
import Counter from './counter'
import Language from './language'
import Events from './events'
import Comments from './comments'
import Snackbar from './snackbar'
import User from './user'

Vue.use(Vuex)

const store = new Vuex.Store({
  // plugins: [vuexLocal.plugin],

  modules: {
    ...extractVuexModule(Language),
    ...extractVuexModule(Counter),
    ...extractVuexModule(Auth),
    ...extractVuexModule(Events),
    ...extractVuexModule(Comments),
    ...extractVuexModule(Snackbar),
    ...extractVuexModule(User)

  },

  actions: {
    async nuxtServerInit({ dispatch }) {
      // await dispatch('events/fetch')
    }
  }
})

const createStore = () => {
  return store
}

//THIS IS REQUIRED
//@ts-ignore
const vxm = {
  counter: createProxy(store, Counter),
  language: createProxy(store, Language),
  auth: createProxy(store, Auth),
  events: createProxy(store, Events),
  comments: createProxy(store, Comments),
  snackbar: createProxy(store, Snackbar),
  user: createProxy(store, User)

  // comments: createProxy(store, Comments)
}

export default createStore
