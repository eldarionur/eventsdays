import { createModule, action, mutation } from 'vuex-class-component'
import { v4 as uuidv4 } from 'uuid'
import _ from 'lodash'
import { finderById, to } from '~/utils/helpers'
import { collectionStream, QueryBuilder } from '~/utils/dbFirestore'
import { EVENTS_NAMESPACE } from './events'

export const CommentsNamespace = 'comments'
const VuexModule = createModule({
  namespaced: CommentsNamespace,
  strict: false,
  target: 'nuxt'
})

export const COMMENTS_NAMESPACE = 'comments'

type Comment = Comments.CommentV2

export default class Comments extends VuexModule {
  private _comments: Comment[] = []
  private _commentsByUser: Comment[] = []


  //*===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙Getters

  get comments() {
    return _.sortBy(this._comments, 'createdAt')
  }

  get commentsByUserId() {
    return this._commentsByUser
  }

  //*===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧Mutations

  @mutation
  private _setComments(comments: Comment[]) {
    this._comments = comments
  }

  @mutation
  private _setCommentsByUser(comments: Comment[]) {
    this._commentsByUser = comments
  }

  //*===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖Actions

  @action
  public async create(p: Comments.CreatePV2): Promise<void> {
    const { text, eventId, replyTo } = p
    //* Pull user info
    const user = this.$store.getters['auth/profile'] as Auth.ProfilePublic
    if (!user) return Promise.reject()

    //* Compose comment
    const comment: Comment = {
      id: uuidv4(),
      text,
      eventId,
      createdAt: new Date().valueOf(),
      userId: user.id,
      userImg: user.img,
      userName: user.displayName
    }

    //* If comment is a reply, save replied user's id
    if (replyTo) {
      comment.replyTo = replyTo
    }

    const reference = this.$store.$fire.firestore.doc(`/comments/${comment.id}`)
    const [err, ___] = await to(reference.set(comment))
    if (err) {
      console.error(err)
      return Promise.reject(err)
    }

    this._setComments([...this.comments, comment])

    //** Update replies count for the event
    const filterByEvent = (c: Comment) => c.eventId == eventId
    const eventComments = this.comments.filter(filterByEvent)
    const replies = eventComments.length

    const data = { eventId, replies }
    this.$store.dispatch(EVENTS_NAMESPACE + '/updateReplies', data)

    return Promise.resolve()
  }

  @action
  public async fetch(p: {
    eventId?: string
    userId?: string
  }): Promise<Comment[]> {
    console.log('Fetching comments for:', p.eventId)

    const { eventId, userId } = p


    //* If event ids are set, return only events that belong there
    let queryBuilder: QueryBuilder | null = null
    queryBuilder = (q) => {
      if (eventId) {
        q = q.where('eventId', '==', eventId)
      }
      if (userId) {
        q = q.where('userId', '==', userId)
      }
      return q
    }

    type T = Comment
    const items: T[] = await collectionStream<T, T>({
      path: '/comments',
      queryBuilder: queryBuilder,
      mapper: (e) => e,
      vueinst: this
    })

    return Promise.resolve(items)
  }



  @action
  public async fetchComments(eventId: string): Promise<any> {
    const comments = await this.fetch({
      eventId: eventId
    })

    //* Save to store
    this._setComments(comments)
  }

  @action
  public async fetchCommentsByUserId(userId: string): Promise<any> {
    const comments = await this.fetch({
      userId: userId
    })

    //* Save to store
    this._setCommentsByUser(comments)
  }



  @action
  public async update(data: Comments.UpdatePV2): Promise<void> {
    const reference = this.$store.$fire.firestore.doc(`/comments/${data.id}`)
    const [err, ___] = await to(reference.update(data))
    const findById = finderById(data.id)
    const comment = this.comments.find(findById)
    if (err || !comment) return Promise.reject()
    comment.text = data.text
    this._setComments([...this.comments])
    return Promise.resolve()
  }

  @action
  public async delete(commentId: string): Promise<void> {
    const reference = this.$store.$fire.firestore.doc(`/comments/${commentId}`)
    //* Dispatch request
    const [err, ___] = await to(reference.delete())
    //* Get comment index from store, delete it
    const findById = finderById(commentId)
    const index = this.comments.findIndex(findById)
    if (err || index < 0) return Promise.reject()

    const eventId = this.comments[index].eventId // Required for Update replies count

    //** Update store - remove comment
    const comments = this.comments
    comments.splice(index, 1)
    this._setComments([...comments])

    //** Update replies count for the event
    const filterByEvent = (c: Comment) => c.eventId == eventId
    const eventComments = this.comments.filter(filterByEvent)
    const replies = eventComments.length
    const data = { eventId, replies }
    this.$store.dispatch(EVENTS_NAMESPACE + '/updateReplies', data)

    return Promise.resolve()
  }
}
