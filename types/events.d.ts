declare namespace Events {
  type Event = {
    id: string
    title: string
    text: string
    img: string
    date: number
    createdAt: number
    location: Location
    organizerId: string
    organizerName: string
    attendees: User[]
    users: EventUser[]
    positionLat: number
    positionLng: number
    replies?: number
    place?: string
  }

  type EventBackend = {
    id: string
    title: string
    text: string
    date: number
    img: string
    createdAt: number
    location: Location
    organizerId: string
    organizerName: string
    attendees: User[]
    users: EventUser[]
    positionLat: number
    positionLng: number
    replies?: number
    place?: string
  }

  type User = {
    id: string
    displayName: string
    name: string
    surname: string
    img: string
  }

  type EventUser = {
    userId: string
    eventId: string
    v: number
  }

  type Location = {}

  enum CommentsOrder {
    best = 'best',
    newest = 'newest',
    oldest = 'oldest'
  }

  export type CreateP = {
    id: string
    title: string
    text: string
    date: number
    positionLat: number
    positionLng: number
    place: string
    img?: string
  }
  type CreateA = (p: CreateP) => Promise<void>
  type FetchP = {
    isPast: boolean
  }
  type FetchA = (p: FetchP) => Promise<void>
  type FetchOrganizedA = () => Promise<void>

  type FetchCommentsA = (p: string) => Promise<void>

  type UploadImgP = { file: File; eventId: string; oldImgPath?: string }
  type UploadImgA = (p: UploadImgP) => Promise<string>
  type AttendP = {
    eventId: string
    v: number
  }
  type AttendA = (p: AttendP) => Promise<void>

  type SortP = { field: string; isAsc: boolean }
  type SortA = (p: SortP) => Promise<void>
}
