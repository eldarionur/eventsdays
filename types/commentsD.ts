import { Decorator } from '.'

//*===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶Secondary

const namespace = 'comments' // ! Important to set correctly
const scope = { namespace }

//*===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰State

interface State {
  records: Comments.Comment[]
}

//*===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙Getters

const Getters = {
  getRecords: 'getRecords'
}

//*===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧Mutations

const Mutations = {
  set: 'set',
  addRecord: 'addRecord',
  update: 'update'
}

//*===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖Actions

const Actions = {
  set: 'set',
  fetch: 'fetch',
  create: 'create',
  getRecords: 'getRecords',
  update: 'getRecords',
  delete: 'delete',
  upvote: 'upvote',
  upvoteReset: 'upvoteReset',
  downvote: 'downvote',
  downvoteReset: 'downvoteReset',
  report: 'report',
  fetchReplies: 'fetchReplies',
  updateSoft: 'updateSoft',
  fetchUserComments: 'fetchUserComments',
  fetchReplyComment: 'fetchReplyComment'
}

//*===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎Decorators

const Decorators: {
  getRecords: Decorator
  set: Decorator
  fetch: Decorator
  create: Decorator
  update: Decorator
  delete: Decorator
  upvote: Decorator
  upvoteReset: Decorator
  downvote: Decorator
  downvoteReset: Decorator
  report: Decorator
  fetchReplies: Decorator
  updateSoft: Decorator
  fetchUserComments: Decorator
  fetchReplyComment: Decorator

  extFetch: string
} = {
  getRecords: [Getters.getRecords, scope],
  set: [Actions.set, scope],
  fetch: [Actions.fetch, scope],
  create: [Actions.create, scope],
  update: [Actions.update, scope],
  delete: [Actions.delete, scope],
  upvote: [Actions.upvote, scope],
  upvoteReset: [Actions.upvoteReset, scope],
  downvote: [Actions.downvote, scope],
  downvoteReset: [Actions.downvoteReset, scope],
  report: [Actions.report, scope],
  fetchReplies: [Actions.fetchReplies, scope],
  updateSoft: [Actions.updateSoft, scope],
  fetchUserComments: [Actions.fetchUserComments, scope],
  fetchReplyComment: [Actions.fetchReplyComment, scope],

  extFetch: namespace + '/' + Actions.fetch
}

//* Sync to .d.ts
export enum CommentsOrder {
  best = 'best',
  newest = 'newest',
  oldest = 'oldest'
}

export const orderOptionsGql = {
  [CommentsOrder.newest]: `{createdAt: desc }`,
  [CommentsOrder.oldest]: `{createdAt: asc }`,
  [CommentsOrder.best]: `{comments_users_upvotes_aggregate: 
    {count: desc}}, {createdAt: desc }`
}

//TODO: selection by best logic is wrong
export const afterOptionsGql = {
  [CommentsOrder.newest]: (c: Comments.Comment) =>
    `{createdAt: {_lte: "${c.createdAt}"}}`,
  [CommentsOrder.oldest]: (c: Comments.Comment) =>
    `{createdAt: {_gte: "${c.createdAt}"}}`,
  [CommentsOrder.best]: (c: Comments.Comment) =>
    `{_and:  [
      {createdAt: { _lte: "${c.createdAt}"}}
      {upvotesCount: { _lte: "${c.upvotesCount}"}}
    ]}`
}

//*===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞Exports
export { State, Getters, Mutations, Actions, Decorators }
