import { Decorators as AuthS } from './authD'
import { Decorators as RootS } from './storeD'
import { Decorators as HistoryS } from './historyD'
import { Decorators as TagsS } from './tagsD'
import { Decorators as CommentsS } from './commentsD'
import { Decorators as EventsS } from './eventsD'

//*===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶Secondary

export type Form = string
export type Decorator = [string, { namespace: string }]

// Sync to auth.d.ts
enum Languages {
  en = 'en',
  zh = 'zh-tw',
  ch = 'zh-cn',
  ru = 'ru'
}
type Language = Languages.en | Languages.zh | Languages.ch | Languages.ru

const Stat = {
  pathRoot: '/',
  PageRefresh: '/refresh'
}

const Events = {
  ShowLoader: 'ShowLoader',
  ShowSnackbar: 'ShowSnackbar',
  EventClaimed: 'EventClaimed',
  FetchComments: 'FetchComments'
}

//*===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞Exports

export {
  AuthS,
  RootS,
  HistoryS,
  TagsS,
  CommentsS,
  EventsS,
  Language,
  Languages,
  Events,
  Stat
}

//NEW STUFF

export interface Router extends Vue {
  
}

// export interface IUsers {
//   [index: number]: {
//     userId: string
//     progress: string
//   }
// }
