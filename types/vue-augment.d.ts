// import { MsgT } from '@/plugins/i18n'
import { config } from '@/custom/config'
import { MsgT } from '~/plugins/i18n/translations'

declare module 'vue/types/vue' {
  interface Vue {
    // $txt: { [key in MsgT]: (v?: Types.TranslationVariables) => string } // List of i18n translations    $showSnack: (text: string, icon?: string) => void
    $showSnack: (text: string, icon?: string) => void
    // $config: any
    i18n: any
    $txt: { [key in MsgT]: (v?: Types.TranslationVariables) => string } // List of i18n translations    $showSnack: (text: string, icon?: string) => void
  }
}
