import { Decorator } from '.'

//*===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶Secondary

const namespace = 'tags' // ! Important to set correctly
const scope = { namespace }

//*===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰State

interface State {
  records: Tags.Record[]
  subs: Tags.Subscription[]
  isSubsFetched: boolean
}

//*===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙Getters

const Getters = {
  getRecords: 'getRecords',
  getSubs: 'getSubsj',
  listTags: 'listTags',
  getIsSubsFetched: 'getIsSubsFetched',
  getSubsTags: 'getSubsTags'
}

//*===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧Mutations

const Mutations = {
  setRecords: 'setRecords',
  push: 'push',
  subsSet: 'subsSet',
  subsPush: 'subsPush',
  subsDelete: 'subsDelete',
  setIsSubsFetched: 'setIsSubsFetched'
}

//*===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖Actions

const Actions = {
  set: 'set',
  subscribe: 'subscribe',
  fetchSubs: 'fetchSubs',
  subsDelete: 'subsDelete'
}

//*===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎Decorators

const Decorators: {
  getRecords: Decorator
  set: Decorator
  listTags: Decorator
  subscribe: Decorator
  getSubs: Decorator
  fetchSubs: Decorator
  subsDelete: Decorator
  getIsSubsFetched: Decorator

  extGetRecords: string
  extFetchSubs: string
  extGetSubs: string
  extGetSubsTags: string
} = {
  getRecords: [Getters.getRecords, scope],
  listTags: [Getters.listTags, scope],
  getSubs: [Getters.getSubs, scope],
  set: [Actions.set, scope],
  subscribe: [Actions.subscribe, scope],
  fetchSubs: [Actions.fetchSubs, scope],
  subsDelete: [Actions.subsDelete, scope],
  getIsSubsFetched: [Getters.getIsSubsFetched, scope],

  extGetRecords: namespace + '/' + Getters.getRecords,
  extGetSubs: namespace + '/' + Getters.getSubs,
  extGetSubsTags: namespace + '/' + Getters.getSubsTags,
  extFetchSubs: namespace + '/' + Actions.fetchSubs
}

//*===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞Exports
export { State, Getters, Mutations, Actions, Decorators }
