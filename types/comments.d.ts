declare namespace Comments {
  type CommentV2 = {
    id: string
    userId: string
    userImg: string
    userName: string
    parentId?: string
    eventId: string
    replyTo?: string // ID of the user that got replied to

    text: string

    createdAt: number
    deletedAt?: number
    isEdited?: boolean
  }

  type CreatePV2 = { text: string; eventId: string; replyTo?: string }
  type CreateAV2 = (p: CreatePV2) => Promise<CommentV2>

  type UpdatePV2 = { text: string; id: string }
  type UpdateAV2 = (p: UpdatePV2) => Promise<CommentV2>

  type DeleteAV2 = (p: string) => Promise<CommentV2>

  //*OLD

  type Comment = {
    id: string
    parentId?: string
    threadId?: string
    depth: number
    text: string
    createdAt: string
    deletedAt: string

    userId: string
    user: User
    upvotes: User[]
    iUpvote: boolean
    iDownvote: boolean
    comments_tags: { tagId: string }[]

    //* Replies count

    replies_aggregate: { aggregate: { count: number } }
    replies: Comment[]
    upvotesCount: number
    computedIsNew?: boolean
    isDemoingReply?: boolean //* Related to showMore btn handling of events
    parent?: Comment
    ignoreShowmore?: boolean //* Related to showMore btn handling of events
  }

  type CommentRaw = {
    id: string
    parentId?: string
    text: string
    createdAt: string

    userI: string
  }

  type User = {
    id: string
    nickname: string
    name: string
    surname: string
    img: string
  }

  type CreateA = (p: CreateP) => Promise<Comment>
  type CreateP = {
    text: string
    tags?: string[]
    parent?: Comment
  }

  type FetchA = (p: FetchP) => Promise<void>
  type FetchP = {
    showMore?: boolean
    userId?: string
  }

  type UpdateA = (p: UpdateP) => Promise<Comment>
  type UpdateP = {
    text: string
    id: string
    tags?: string[]
  }
  type UpdateSoftA = (p: Comment) => Promise<Comment>
  type FetchRepliesA = (comment: Comment) => Promise<void>

  type DeleteA = (p: DeleteP) => Promise<void>
  type DeleteP = { id: string }

  type UpvoteA = (p: UpvoteP) => Promise<void>
  type UpvoteP = { id: string }

  type UpvoteResetA = (p: UpvoteResetP) => Promise<void>
  type UpvoteResetP = { commentId: string; userId: string }

  type ReportA = (p: ReportP) => Promise<void>
  type ReportP = { commentId: string; text: string }

  enum CommentsOrder {
    best = 'best',
    newest = 'newest',
    oldest = 'oldest'
  }
}
