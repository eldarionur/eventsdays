declare namespace Auth {
  export type User = {
    id: string
    displayName: string
    email: string
    phone: string
    img: string
    isVerified?: boolean
  } | null

  export type ProfilePublic = {
    id: string
    img: string
    displayName: string
    isModerated?: boolean
  } | null

  //* Same as SignupP. Password should be hashed
  export type AuthedUser = {
    id: string
    email: string
    displayName: string
    password: string
    accessToken?: string
    refreshToken?: string
  } | null

  export type ActionLogout = () => Promise<null>
  export type ActionSetLang = (lang: Language) => Promise<null>

  export type UpdateUserA = (p: UpdateUserP) => Promise<void>
  type UpdateUserP = { name: string; surname: string }

  export type CreateUserA = (p: CreateUserP) => Promise<void>
  type CreateUserP = {
    name: string
    surname: string
    displayName: string
    email: string
    img: string
    password: string
    language: Language
  }
  export type UpdateUserImgA = (p: File) => Promise<void>
  export type UpdateInfoA = (p: UpdateInfoP) => Promise<void>

  type UpdateInfoP = {
    displayName: string
    name: string
    surname: string
  }

  type NewUser = {
    name: string
    surname: string
    displayName: string
    email: string
    img: string
    language: Language
  }

  type UserPublic = {
    id: string
    name: string
    surname: string
    displayName?: string
    img: string
  }

  // Sync to index.ts
  enum Languages {
    en = 'en',
    zh = 'zh-tw',
    ch = 'zh-cn',
    ru = 'ru'
  }
  type Language = Languages.en | Languages.zh | Languages.ch | Languages.ru

  type FetchUserA = (p: { id: string }) => Promise<UserPublic>

  export type SigninGoogleP = {
    email: string
    password: string
    localId: string
    displayName?: string
    refreshToken?: string
    idToken?: string
  }
  export type SigninGoogleA = () => Promise<void>
  export type SignupAnonA = () => Promise<void>

  export type SignupP = {
    email: string
    password: string
    idToken?: string
  }
  type SignupA = (p: SignupP) => Promise<void>

  export type RecoverPasswordP = { email: string }
  type RecoverPasswordA = (p: RecoverPasswordP) => Promise<void>

  type SetCommentsOrderA = (p: Comments.CommentsOrder) => Promise<void>

  // *NEW

  export type SigninP = {
    email: string
    password: string
    displayName?: string
    refreshToken?: string
  }
  type SigninA = (p: SigninP) => Promise<void>

  export type SignoutP = {
    nonauthed?: boolean
  }
  type SignoutA = (p: SignoutP) => Promise<void>

  //* Update profile
  export type UpdateProfileP = {
    displayName: string
    img?: string
  }
  type UpdateProfileA = (p: UpdateProfileP) => Promise<void>

  type UploadImgP = { file: File; name: string; oldImgPath?: string }
  type UploadImgA = (p: UploadImgP) => Promise<string>
}
