// ! Warning: collision with default History item

type Dictionary<T> = { [key: string]: T }

declare namespace History {
  type Item = {
    name: string
    query: Dictionary<string | (string | null)[]>
    params: Dictionary<string>
    isShallow?: boolean
  }

  type GoBackA = (isShallow?: boolean) => Promise<void>
  type PushA = (p: Item) => Promise<void>
}

type HistoryItem = History.Item
