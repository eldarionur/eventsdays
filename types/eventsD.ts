import { Decorator } from '.'

//*===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶Secondary

const namespace = 'events' // ! Important to set correctly
const scope = { namespace }

//*===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰State

interface State {
  records: Events.Event[]
  countUnread: number
}

//*===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙Getters

const Getters = {
  getRecords: 'getRecords',
  getCountUnread: 'getCountUnread'
}

//*===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧Mutations

const Mutations = {
  set: 'set',
  setCountUnread: 'setCountUnread',
  addRecord: 'addRecord',
  update: 'update'
}

//*===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖Actions

const Actions = {
  fetch: 'fetch',
  updateComment: 'updateComment',
  markRead: 'markRead',
  countUnread: 'countUnread'
}

//*===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎Decorators

const Decorators: {
  getRecords: Decorator
  fetch: Decorator
  updateComment: Decorator
  markRead: Decorator
  countUnread: Decorator
  getCountUnread: Decorator

  extFetch: string
  extCountUnread: string
} = {
  getRecords: [Getters.getRecords, scope],
  fetch: [Actions.fetch, scope],
  updateComment: [Actions.updateComment, scope],
  markRead: [Actions.markRead, scope],
  countUnread: [Actions.countUnread, scope],
  getCountUnread: [Getters.getCountUnread, scope],

  extFetch: namespace + '/' + Actions.fetch,
  extCountUnread: namespace + '/' + Actions.countUnread
}

//*===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞Exports

export { State, Getters, Mutations, Actions, Decorators }
