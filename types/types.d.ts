//* General types file

declare namespace Types {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  type TodoFixMe = any //* Used to clear out eslint warnings, bad practice

  type FormRule = {
    [key in string]: { min: number; max: number }
  }

  type Rule = (v: string) => string | true // * Validation rule

  //* Sync to obj at stxForms
  enum FormInputTypes {
    textfield = 'textfield',
    password = 'password',
    passwordConfirm = 'passwordConfirm',
    textarea = 'textarea',
    select = 'select',
    iconSelect = 'iconSelect',
    switch = 'switch',
    checkbox = 'checkbox',
    image = 'image',
    number = 'number',
    positionLat = 'positionLat',
    positionLng = 'positionLng',
    date = 'date',
    dateRange = 'dateRange',
    tags = 'tags',
    time = 'time'
  }

  interface FormField {
    name?: string
    label?: string
    title?: string
    inputType: FormInputTypes
    rules?: string
    inputOptions?: (
      | string
      | number
      | { id: string; text: string }
      | Record<string, TodoFixMe>
    )[]
    value?: TodoFixMe // Default value
    hidden?: boolean
    isUpdated?: boolean
    disabled?: boolean
    selectorText?: string
    selectorValue?: string
    placeholder?: string

    noEdit?: boolean

    isOpen?: boolean //* If uploading image - opens form
    imgHeight?: number
    imgWidth?: number
    imgTitle?: string
    imgSubtitle?: string
    imgRatioX?: number
    imgRatioY?: number

    dialogTitle?: string
    dialogSubtitle?: string

    connectedField?: string // TODO: deprecate in favor of "secondary"
    secondary?: TodoFixMe // <- Contains object - connected form
  }

  export interface Form extends Element {
    validate: () => null
    resetValidation: () => null
  }

  type RootEl = TodoFixMe
  type Timeout = number | undefined | TodoFixMe
  type GqlObserver = TodoFixMe
  type RoutesItem = TodoFixMe

  interface VeeObserver extends Element {
    reset(): () => void
    validate(): () => void
  }

  type RecordAny = TodoFixMe
  type FormDataAny = TodoFixMe
  type FormData = TodoFixMe
  type FilePromiseA = (p: File) => Promise<void>
  type Forms = { [key in string]: Types.FormField }
  type Position = { lat: number; lng: number }

  type InputValueGeneric = number | string | string[] | File

  interface VMenu extends Element {
    save: (p: string | string[]) => void
  }

  type Icon = TodoFixMe
  type TranslationVariables = { [key in string]: string | number }
  type TargetAny = TodoFixMe

  type ResponseError = { code: string; message: string }

  type GMap = {
    setZoom: (zoom: number) => void
    panTo: (coords: Position) => void
  }

  interface GMapRef extends Element {
    $mapPromise: Promise<GMap>
  }

  type GqlVariables = { [key in string]: string | string[] }
  type StringMap = { [key in string]: string }

  interface VueRef extends Element {
    $el: HTMLElement
  }
  interface VTextarea extends Element {
    focus: () => {}
  }

  type ReqUrl = { method: string; path: string }

  type AxiosDummy = TodoFixMe
  type IterableOptions = {
    [key in any]: any
  }

  type Iterable = 'Comment' | 'Event'
}
