//*===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰State

export interface State {
  isLoading?: boolean //* Sample value
}

//*===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙Getters

const Getters = {}

//*===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧Mutations

const Mutations = {}

//*===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖Actions

const Actions = {
  resetAll: 'resetAll',
  initApp: 'initApp',
  myxios: 'myxios'
}

//*===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎Decorators

const Decorators: {
  initApp: string
  resetAll: string
  myxios: string
} = {
  initApp: Actions.initApp,
  resetAll: Actions.resetAll,
  myxios: Actions.myxios
}

//*===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰Exports

export { Getters, Mutations, Actions, Decorators }
