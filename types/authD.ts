import { Decorator } from '.'

//*===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶Secondary

// Namespace and scope helpers
const namespace = 'auth' // ! Important to set correctly
const scope = { namespace }

//*===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰State

export interface State {
  user: Auth.User
  token: string
  lang: Auth.Language
  commentsOrder: Comments.CommentsOrder
  authedUser: Auth.AuthedUser
}

//*===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙Getters

const Getters = {
  getUser: 'getUser',
  getToken: 'getToken',
  getLang: 'getLang',
  isLogged: 'isLogged',
  isUserExpired: 'isUserExpired',
  getCommentsOrder: 'getCommentsOrder',
  getAuthedUser: 'getAuthedUser'
}

//*===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧Mutations

const Mutations = {
  updateToken: 'updateToken',
  setUser: 'setUser',
  updateUser: 'updateUser',
  setLang: 'setLang',
  setToken: 'setToken',
  setCommentsOrder: 'setCommentsOrder',
  setAuthedUser: 'setAuthedUser'
}

//*===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖Actions

const Actions = {
  handleLoginResponse: 'handleLoginResponse',
  logout: 'logout',
  reset: 'reset',
  updateUser: 'updateUser',
  setLang: 'setLang',
  setToken: 'setToken',
  createUser: 'createUser',
  updateUserImg: 'updateUserImg',
  setUser: 'setUser',
  fetchUser: 'fetchUser',
  setCommentsOrder: 'setCommentsOrder',
  updateInfo: 'updateInfo',
  signup: 'signup',
  signin: 'signin',
  initApollo: 'initApollo',
  fetchUserDetails: 'fetchUserDetails',
  refreshToken: 'refreshToken',
  signinGoogle: 'signinGoogle',
  sendForgotEmail: 'sendForgotEmail',
  signupAnon: 'signupAnon',
  createUserDetails: 'createUserDetails',
  updateUserDetails: 'updateUserDetails'
}

//*===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎Decorators

const Decorators: {
  getUser: Decorator
  getLang: Decorator
  isLogged: Decorator
  logout: Decorator
  updateUser: Decorator
  setLang: Decorator
  isUserExpired: Decorator
  setToken: Decorator
  createUser: Decorator
  updateUserImg: Decorator
  fetchUser: Decorator
  getCommentsOrder: Decorator
  setCommentsOrder: Decorator
  updateInfo: Decorator
  signup: Decorator
  signin: Decorator
  getAuthedUser: Decorator
  signinGoogle: Decorator
  sendForgotEmail: Decorator
  signupAnon: Decorator
  getToken: Decorator
  createUserDetails: Decorator
  updateUserDetails: Decorator

  extReset: string
  extGetUser: string
  extIsLogged: string
  extGetLang: string
  extSetToken: string
  extGetToken: string
  extLogout: string
  extGetCommentsOrder: string
  extInitApollo: string
  extFetchUserDetails: string
  extIsAuthUser: string
  extSigninGoogle: string
} = {
  getUser: [Getters.getUser, scope],
  getToken: [Getters.getToken, scope],
  isLogged: [Getters.isLogged, scope],
  getLang: [Getters.getLang, scope],
  getCommentsOrder: [Getters.getCommentsOrder, scope],
  getAuthedUser: [Getters.getAuthedUser, scope],
  isUserExpired: [Getters.isUserExpired, scope],

  // Actions
  updateInfo: [Actions.updateInfo, scope],
  logout: [Actions.logout, scope],
  updateUser: [Actions.logout, scope],
  setLang: [Actions.setLang, scope],
  setToken: [Actions.setToken, scope],
  createUser: [Actions.createUser, scope],
  updateUserImg: [Actions.updateUserImg, scope],
  fetchUser: [Actions.fetchUser, scope],
  setCommentsOrder: [Actions.setCommentsOrder, scope],
  signup: [Actions.signup, scope],
  signin: [Actions.signin, scope],
  signinGoogle: [Actions.signinGoogle, scope],
  sendForgotEmail: [Actions.sendForgotEmail, scope],
  signupAnon: [Actions.signupAnon, scope],
  createUserDetails: [Actions.createUserDetails, scope],
  updateUserDetails: [Actions.updateUserDetails, scope],

  // Externals
  extReset: namespace + '/' + Actions.reset,
  extGetUser: namespace + '/' + Getters.getUser,
  extIsLogged: namespace + '/' + Getters.isLogged,
  extGetLang: namespace + '/' + Getters.getLang,
  extSetToken: namespace + '/' + Actions.setToken,
  extGetToken: namespace + '/' + Getters.getToken,
  extGetCommentsOrder: namespace + '/' + Getters.getCommentsOrder,
  extIsAuthUser: namespace + '/' + Getters.getAuthedUser,
  extLogout: namespace + '/' + Actions.logout,
  extInitApollo: namespace + '/' + Actions.initApollo,
  extFetchUserDetails: namespace + '/' + Actions.fetchUserDetails,
  extSigninGoogle: namespace + '/' + Actions.signinGoogle
}

//*===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰Exports

export { Getters, Mutations, Actions, Decorators }
