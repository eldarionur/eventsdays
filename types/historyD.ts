import { Decorator } from '.'

//*===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶Secondary

const namespace = 'history' // ! Important to set correctly
const scope = { namespace }

//*===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰===📰State

interface State {
  items: History.Item[]
}

//*===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙===🌙Getters

const Getters = {
  getItems: 'getItems',
  getCurrent: 'getCurrent'
}

//*===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧===🔧Mutations

const Mutations = {
  set: 'set',
  push: 'push',
  pop: 'pop'
}

//*===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖Actions

const Actions = {
  reset: 'reset',
  push: 'push',
  goBack: 'goBack'
}

//*===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎===💎Decorators

const Decorators: {
  getItems: Decorator
  getCurrent: Decorator
  goBack: Decorator
  push: Decorator

  extReset: string
  extPush: string
  extGoBack: string
} = {
  getCurrent: [Getters.getCurrent, scope],
  getItems: [Getters.getItems, scope],
  goBack: [Actions.goBack, scope],
  push: [Actions.push, scope],

  extReset: namespace + '/' + Actions.reset,
  extPush: namespace + '/' + Actions.push,
  extGoBack: namespace + '/' + Actions.goBack
}

//*===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞===🗞Exports

export { State, Getters, Mutations, Actions, Decorators }
