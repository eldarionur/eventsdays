declare namespace Store {
  // export interface SnackbarMessage {
  //   icon?: string
  //   text: string
  // }

  // export interface SnackMsgData {
  //   type: string
  //   data: any
  // }

  export interface Snack {
    icon?: string
    text?: string
    data?: string | number
  }

  type PathMethod = 'get' | 'post' | 'put' | 'delete'

  type Path = {
    path: string
    method: PathMethod
  }

  export type MyxoisP = {
    url: Path
    hideError?: boolean
    data: unknown
    onSuccess: (responseData: unknown) => {}
    onError: () => {}
    onFinish: () => {} // Executed either success or error
    options?: {}
    headers: any
  }
}
