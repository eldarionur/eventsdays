declare namespace Tags {
  type Record = Tag
  type Tag = string

  type Sub = {
    id: string
    createdAt: string
    subscriptions_tags: {
      tagId: string
    }[]
  }
  type Subscription = Sub

  //*===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖===🤖Actions

  type SetA = (p: string[]) => Promise<Record[]>
  type SubscribeA = () => Promise<void>
  type SubsDeleteA = (id: string) => Promise<void>
  type SubsFetchA = () => Promise<Sub[]>
}
