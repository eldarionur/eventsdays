// TODO: rename stx to user

import { Languages } from '@/types'

const env = process.env.VUE_APP_ENV
const allowedLang: Languages[] = [
  Languages.en,
  Languages.zh,
  Languages.ch,
  Languages.ru
]
const versionHash = process.env.VUE_APP_GIT_HASH
export const URL_TOKEN_REFRESH = process.env.VUE_APP_URL_TOKEN_REFRESH || ''
const refreshUrl = process.env.VUE_APP_TOKEN_REFRESH_URL || '' //TODO: depr for tokenRefreshUrl
const getRefreshUrl = (uid: string) => `${refreshUrl}?uid=${uid}`
const userDefaultImg = '/img/user-img-default.jpg'
const isStoreDebug = true
const notFoundImg = '/profile-img.jpg'
export const authUrl = 'https://identitytoolkit.googleapis.com/v1'
// export const authApiKey = 'Sausage'
export const authApiKey = 'AIzaSyBYT9Qav11og1_w7HVyJDosaFb611DQ0eo' //TODO: depr
export const AUTH_API_KEY = 'AIzaSyBYT9Qav11og1_w7HVyJDosaFb611DQ0eo'

export const COMMENTS_PER_PAGE = 20
export const EVENTS_PER_PAGE = 20

export const CONFIG_FIREBASE = {
  apiKey: process.env.VUE_APP_FIREBASE_API_KEY,
  authDomain: process.env.VUE_APP_FIREBASE_DOMAIN,
  projectId: process.env.VUE_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.VUE_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.VUE_APP_FIREBASE_MESSAGING_SENDER_ID
}

const iterables: { [key in Types.Iterable]: string } = {
  Comment: 'Comment',
  Event: 'Event'
}

const allowedEnv = {
  chromeExtension: 'chrome-extension',
  web: 'web'
}

const isChromeExtension = () => env === allowedEnv.chromeExtension

const TextLoginToVote = 'Please login in order to vote'

const RULES = {
  tagLengthMin: 3,
  tagLengthMax: 30
}

const mapCenterIstanbul = {
  lat: 41.015137,
  lng: 28.97953
}

export const EVENT_COMPLETED_DEADLINE = 1.5 // in HOURS

export const config = {
  env,
  allowedLang,
  versionHash,
  authUrl,
  getRefreshUrl,
  isGqlDebug: true,
  pageSize: 5,
  userDefaultImg,
  isStoreDebug,
  notFoundImg,
  iterables,
  allowedEnv,
  TextLoginToVote,
  isChromeExtension,
  RULES,
  mapCenterIstanbul
}
