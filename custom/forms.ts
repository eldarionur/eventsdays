//*===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶Secondary

import { myi18n } from '~/plugins/i18n'
import { Msg } from '~/plugins/i18n/translations'

//* Sync to obj at types.d.ts
export enum FormInputTypes {
  textfield = 'textfield',
  password = 'password',
  passwordConfirm = 'passwordConfirm', //* In order to work password field should have ref="password"
  textarea = 'textarea',
  select = 'select',
  iconSelect = 'iconSelect',
  switch = 'switch',
  checkbox = 'checkbox',
  image = 'image',
  number = 'number',
  positionLat = 'positionLat',
  positionLng = 'positionLng',
  date = 'date',
  dateRange = 'dateRange',
  tags = 'tags',
  time = 'time'
}

// const t = (msg: any) => String(msg)
//todo:i18n
const t = (msg: Msg) => String(myi18n.t(msg))

const rules = {
  eventTitle: 'required|min:6|max:30',
  eventText: 'required|min:20|max:180',
  password: 'required|min:6|max:30',
  userName: 'required|min:2|max:50',
  userSurname: 'required|min:2|max:50',
  nickname: 'required|min:5|max:50',
  positionLat: 'required|min:-90|max:90',
  positionLng: 'required|min:-180|max:180',
  required: 'required',
  displayName: 'required|min:5|max:50'
}

//*===🔷===🔷===🔷===🔷===🔷===🔷===🔷===🔷===🔷===🔷===🔷===🔷Primary

export type FormSigninFields = 'email' | 'password'
export type FormSignin = { [key in FormSigninFields]: Types.FormField }
const signin = (): FormSignin => {
  return {
    email: {
      value: '',
      inputType: FormInputTypes.textfield,
      label: t(Msg.Email),
      rules: 'required|email'
    },
    password: {
      value: '',
      inputType: FormInputTypes.password,
      label: t(Msg.Password),
      rules: rules.password
    }
  }
}

export type FormForgotFields = 'email'
export type FormForgot = { [key in FormForgotFields]: Types.FormField }
const forgot = (): FormForgot => {
  return {
    email: {
      value: '',
      inputType: FormInputTypes.textfield,
      label: t(Msg.Email),
      rules: 'required|email'
    }
  }
}

export type FormSignupFields =
  | 'email'
  | 'password'
  | 'passwordConfirm'
  | 'isTosAccept'
export type FormSignup = { [key in FormSignupFields]: Types.FormField }
const signup = (): FormSignup => {
  return {
    email: {
      value: '',
      inputType: FormInputTypes.textfield,
      label: t(Msg.Email),
      rules: 'required|email'
    },
    password: {
      value: '',
      inputType: FormInputTypes.password,
      label: t(Msg.Password),
      rules: rules.password
    },
    passwordConfirm: {
      value: '',
      inputType: FormInputTypes.passwordConfirm,
      label: t(Msg.PasswordConfirm),
      rules: 'required|password:@password'
    },
    isTosAccept: {
      value: false,
      inputType: FormInputTypes.checkbox,
      label: 'I accept Terms of Service and Privacy Policy',
      rules: 'requiredTos'
    }
  }
}

//* Form for create/update
export type FormSettingsFields = 'name' | 'surname' | 'displayName'
export type FormSettings = { [key in FormSettingsFields]: Types.FormField }

const settings = (): FormSettings => {
  return {
    name: {
      value: '',
      inputType: FormInputTypes.textfield,
      label: t(Msg.Name),
      rules: rules.userName
    },
    surname: {
      value: '',
      inputType: FormInputTypes.textfield,
      label: t(Msg.Surname),
      rules: rules.userSurname
    },
    displayName: {
      value: '',
      inputType: FormInputTypes.textfield,
      label: 'Display name',
      rules: rules.displayName
    }
  }
}

export type FormEventFields =
  | 'title'
  | 'text'
  | 'date'
  | 'time'
  | 'img'
  | 'place'
  | 'positionLat'
  | 'positionLng'
// | 'img'
export type FormEvent = { [key in FormEventFields]: Types.FormField }

const event = (): FormEvent => {
  var time = new Date()
  time.setHours(0)
  time.setMinutes(0)
  return {
    title: {
      value: '',
      inputType: FormInputTypes.textfield,
      label: t(Msg.Title),
      rules: rules.eventTitle
    },
    text: {
      value: '',
      inputType: FormInputTypes.textarea,
      label: t(Msg.Description),
      rules: rules.eventText
    },
    date: {
      value: new Date(),
      inputType: FormInputTypes.date,
      label: 'Date',
      rules: rules.required
    },
    time: {
      value: time,
      inputType: FormInputTypes.time,
      label: 'Time',
      rules: rules.required
    },

    img: {
      value: null,
      inputType: FormInputTypes.image,
      label: t(Msg.Image),
      placeholder: t(Msg.PickAnImage),
      rules: 'required',
      isOpen: false,
      imgHeight: 150,
      imgWidth: 300,
      imgTitle: 'Update event logo',
      imgSubtitle:
        'Image should have size of atleast 300 pixels for width and 150 for height',
      imgRatioX: 2,
      imgRatioY: 1
    },

    place: {
      value: '',
      inputType: FormInputTypes.textfield,
      label: 'Place Address',
      rules: rules.required
    },

    positionLat: {
      value: '',
      inputType: FormInputTypes.positionLat,
      label: 'Set Location',
      isOpen: false,
      dialogTitle: 'Location',
      dialogSubtitle: 'Set location of the shop on the map',
      connectedField: 'positionLng',
      rules: rules.positionLat
    },
    positionLng: {
      value: '',
      inputType: FormInputTypes.positionLng,
      label: 'Set Location',
      isOpen: false,
      dialogTitle: 'Location',
      dialogSubtitle: 'Set location of the shop on the map',
      connectedField: 'positionLat',
      rules: rules.positionLng
    }
  }
}

export type FormSuperFields =
  | 'title'
  | 'text'
  | 'date'
  | 'isGood'
  | 'selectFruit'
export type FormSuper = { [key in FormSuperFields]: Types.FormField }
const superForm = (): FormSuper => {
  var time = new Date()
  time.setHours(0)
  time.setMinutes(0)
  return {
    title: {
      value: '',
      inputType: FormInputTypes.textfield,
      label: t(Msg.Title),
      rules: rules.eventTitle
    },
    text: {
      value: '',
      inputType: FormInputTypes.textarea,
      label: t(Msg.Text),
      rules: rules.eventText
    },
    date: {
      value: new Date(),
      inputType: FormInputTypes.date,
      label: 'Date',
      rules: rules.required
    },

    isGood: {
      value: '',
      inputType: FormInputTypes.checkbox,
      label: 'Is Good',
      rules: rules.required
    },
    selectFruit: {
      value: '',
      inputType: FormInputTypes.select,
      label: 'What fruit do you,  like',
      inputOptions: ['Banana', 'Apple', 'Sausage'],
      rules: rules.required
    }
  }
}

export type FormAccountSettingsFields = 'displayName' | 'img'
export type FormAccountSettings = {
  [key in FormAccountSettingsFields]: Types.FormField
}

const accountSettings = (): FormAccountSettings => {
  return {
    displayName: {
      value: '',
      inputType: FormInputTypes.textfield,
      label: 'Name/surname',
      rules: rules.displayName
    },
    img: {
      value: null,
      inputType: FormInputTypes.image,
      label: t(Msg.Image),
      placeholder: t(Msg.PickAnImage),
      rules: 'required',
      isOpen: false,
      imgHeight: 300,
      imgWidth: 300,
      imgTitle: t(Msg.ImgTitle),
      imgSubtitle:
        t(Msg.ImgSubtitle),
      imgRatioX: 1,
      imgRatioY: 1
    }

    // img: {
    //   value: null,
    //   inputType: FormInputTypes.image,
    //   label: t(Msg.Image),
    //   placeholder: t(Msg.PickAnImage),
    //   rules: 'required',
    //   isOpen: false,
    //   imgHeight: 600,
    //   imgWidth: 600,
    //   imgTitle: 'Update profile image',
    //   imgSubtitle:
    //     'Image should have size of atleast 600 pixels for width and 300 for height',
    //   imgRatioX: 2,
    //   imgRatioY: 1
    // }
  }
}

//*===💀===💀===💀===💀===💀===💀===💀===💀===💀===💀===💀===💀Export

const Forms = {
  signin,
  signup,
  forgot,
  settings,
  event,
  superForm,
  accountSettings
}

export default Forms
