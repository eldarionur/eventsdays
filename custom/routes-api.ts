// * === === === === === === === === === ===
// * Backend

// * Specific to firebase auth

export const FIREBASE_ERRORS = {
  MISSING_PASSWORD: 'MISSING_PASSWORD',
  EMAIL_EXISTS: 'EMAIL_EXISTS'
}

// * Method Helper
export const PathMethod = {
  get: 'get',
  put: 'put',
  patch: 'patch',
  post: 'post',
  delete: 'delete'
}
// * Helpers to compose route paths
// const makeRoute = (method: string, pathPart: string) => {
//   const path = `${authUrl}/${pathPart}?key=${authApiKey}`
//   return { method, path }
// }

// const goGet = (path: string) => makeRoute(PathMethod.get, path)
// const goPost = (path: string) => makeRoute(PathMethod.post, path)
// const goPut = (path: string) => makeRoute(PathMethod.put, path)
// const goDelete = (path: string) => makeRoute(PathMethod.delete, path)

export const routesApi = {
  // Authed user
  user: {
    updateUserImage: (name: string) => {
      return {
        method: PathMethod.post,
        path: `` //TODO:storage path
      }
    }
  }
}
