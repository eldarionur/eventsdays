import { Context, Inject } from '@nuxt/types/app'
import Vue from 'vue'
import { Msg, MsgT, translations } from './i18n/translations'

export const i18nLocales = ['en', 'ru']

export const i18nConfig = {
  locales: i18nLocales,
  defaultLocale: 'en',
  vueI18n: {
    fallbackLocale: 'en',
    messages: translations
  }
}

var myi18n: any = null

// ** Translations - convert enum to key->translation object
export default (context: Context, inject: Inject) => {
  // export default (context, inject) => {
  Vue.prototype.$msg = Msg
  Vue.prototype.$xoxo = context
  myi18n = (context as any).i18n
  type TranslationVal = (v?: Types.TranslationVariables) => string
  type TranslationsConverted = {
    [key in MsgT]: TranslationVal | string
  }

  // ** Translations - convert enum to key->translation object
  //* Store translations within txt
  const $txt: TranslationsConverted = { ...MsgT }
  Object.keys(MsgT).forEach((key: string) => {
    if (key in MsgT) {
      const keyWithVars = Msg[key as MsgT] // Translate key from enum to code
      $txt[key as MsgT] = (v: Types.TranslationVariables = {}) =>
        String(myi18n.t(keyWithVars, v))
    }
  })
  inject('txt', $txt)
  // Vue.prototype.$txt = $txt
}

export { myi18n }
