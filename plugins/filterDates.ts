import { Context, Inject } from '@nuxt/types/app'
import Vue from 'vue'
import moment from 'moment/moment'

export default (context: Context, inject: Inject) => {
  Vue.filter('formatAgo', (value: string) => {
    if (!value) return ''
    const date = moment.utc(value).local()
    return date.fromNow()
  })

  Vue.filter('formatEventDate', (value: string) => {
    if (!value) return ''
    moment.locale('en')
    return moment(value).format('MMMM D, ddd')
  })

  Vue.filter('formatEventHour', (value: string) => {
    if (!value) return ''
    const date = moment(value).format('HH:mm')
    return date
  })
}
