


import { Context, Inject } from '@nuxt/types/app'

export default (context: Context, inject: Inject) => {
  inject('notifier', {
    showMessage({ content = '', color = '' }) {
      context.store.dispatch('snackbarNamespace/showSnackbar', { content, color })
    }
  })
}

