// ~/plugins/vuex-persist.js
import VuexPersistence from 'vuex-persist'

export default ({ store }: any) => {
  new VuexPersistence({
    storage: window.localStorage,
    modules: ['language', 'auth', 'counter'],
    supportCircular: true

    /* your options */
  }).plugin(store)
}
