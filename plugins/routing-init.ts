import { Context, Inject } from '@nuxt/types/app'

//! Dupe from routing.js
export default (context: Context, inject: Inject) => {
  if (!process.client) return //?Exn

  setTimeout(() => {
    const path = context.route.name || ''

    const pathName = path.split('___')[0]
    const settingsPage = 'account-details'
    const logoutPage = 'logout'
    if (pathName != settingsPage && pathName != logoutPage) {
      context.store.dispatch('auth/checkProfile')
    }
  }, 100)
}
