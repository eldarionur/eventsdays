import { Context, Inject } from '@nuxt/types/app'
import Vue from 'vue'

export default (context: Context, inject: Inject) => {
  Vue.filter('upper', (value: string) => {
    if (!value) return ''
    return value.toUpperCase()
  })
}
