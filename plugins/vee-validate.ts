import { extend, localize } from 'vee-validate'
import {
  required,
  email,
  min,
  max,
  alpha,
  alpha_num as alhpaNum
} from 'vee-validate/dist/rules'
import { Msg } from '~/plugins/i18n/translations'

const dictionary = {
  en: {
    messages: {
      required: () => '* Required'
    }
  }
}

export default (context: any, __: any) => {
  var myi18n = context.i18n

  const t = (msg: Msg) => String(myi18n.t(msg))

  extend('required', required)

  extend('requiredTos', {
    validate: (value) => !!value,
    message: t(Msg.VeeAcceptTos)
  })

  extend('max', {
    ...max,
    message: t(Msg.VeeMax)
  })

  extend('min', {
    ...min,
    message: t(Msg.VeeMin)
  })

  extend('email', {
    ...email,
    message: t(Msg.VeeEmail)
  })

  extend('alpha', {
    ...alpha,
    message: t(Msg.VeeAlpha)
  })

  extend('alpha_num', {
    ...alhpaNum,
    message: t(Msg.VeeAlphaNum)
  })

  extend('truthy', {
    validate: (value) => !!value,
    message: t(Msg.VeeTruthy)
  })

  //! BAD TYPING
  extend('password', {
    params: ['target'],
    validate: (value, { target }: Types.TargetAny) => value === target,
    message: t(Msg.VeePassword)
  })

  //! BAD TYPING
  extend('periodTo', {
    params: ['target'],
    validate: (value, { target }: Types.TargetAny) => value === target,
    message: '"Period To" is required'
  })

  localize(dictionary)
}
