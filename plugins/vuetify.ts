// import Vue from 'vue'
// import Vuetify from 'vuetify/lib'
// import colors from 'vuetify/es5/util/colors'

// // Vue.use(Vuetify, {
// //   iconfont: 'md',
// // });

// Vue.use(Vuetify, {
//   theme: {
//     primary: '#727375', // a color that is not in the material colors palette
//     secondary: '#7f8285', // a color that is not in the material colors palette
//     accent: '#cc2531',
//     info: colors.teal.lighten1,
//     warning: colors.amber.base,
//     error: colors.deepOrange.accent4,
//     success: colors.green.accent3,
//     white: '#ffffff',
//     subtoolbar: '#3b6eb4',
//     primaryToolbar: '#315f9a',
//     secondaryToolbar: '#4393f9'
//   },
//   options: {
//     customProperties: true
//   }
// })

import '@mdi/font/css/materialdesignicons.css'
import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

const navbarBtnActive = '#00cf83'
const iconfont: 'mdi' = 'mdi'

//*===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶===🔶Secondary
const lightBlue = '#1ea0ff'
const textRegular = '#4c4c4c'
const tagColor = '#297eff'
const casualevents = '#00bfbf'
const casualeventsDark = '#bf2602'

/* Potential colors:
Couch: 008074ff
*/
const opts = {
  icons: {
    iconfont
  },

  theme: {
    options: {
      customProperties: true
    },
    dark: false,
    themes: {
      light: {
        primary: lightBlue,
        secondary: '#7f8285',
        tag: tagColor,
        accent: '#b71c1b',
        white: '#ffffff',
        subtoolbar: '#3b6eb4',
        primaryToolbar: '#315f9a',
        secondaryToolbar: '#4393f9',
        lighty: '#1e1e1e',
        lightyHl: '#000000',
        navbarBtnActive,
        lightBlue,
        textRegular,
        casualevents,
        casualeventsDark
      },
      dark: {
        primary: '#333434', // a color that is not in the material colors palette
        secondary: '#7f8285', // a color that is not in the material colors palette
        accent: '#cc2531',
        white: '#ffffff',
        subtoolbar: '#3b6eb4',
        primaryToolbar: '#315f9a',
        secondaryToolbar: '#4393f9',
        lighty: '#1e1e1e',
        lightyHl: '#000000',
        navbarBtnActive,
        lightBlue,
        textRegular,
        casualevents,
        casualeventsDark
      }
    }
  }
}

Vue.use(Vuetify)

export default new Vuetify(opts)
