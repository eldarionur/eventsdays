module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    webextensions: true
  },
  extends: [
    // "@nuxtjs/eslint-config-typescript",
    // "plugin:nuxt/recommended",

    'plugin:vue/essential',
    'eslint:recommended',
    '@vue/typescript/recommended',
    '@vue/prettier',
    '@vue/prettier/@typescript-eslint'
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    quotes: [0, 'single'],
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',

    'prettier/prettier': [
      'warn',
      {
        singleQuote: true,
        semi: false,
        trailingComma: 'none',
        htmlWhitespaceSensitivity: 'ignore'
      }
    ]
  },
  globals: {
    chrome: true
  }
}
