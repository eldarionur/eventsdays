import { Context } from '@nuxt/types/app'

//! Dupe at plugins/routing-init.ts

export default function (context: Context) {
  if (!process.client) return //?Exn
  const path = context.route.name
  if (!path) return

  const pathName = path.split('___')[0]
  const settingsPage = 'account-details'
  const logoutPage = 'logout'
  if (pathName != settingsPage && pathName != logoutPage) {
    context.store.dispatch('auth/checkProfile')
  }
}
