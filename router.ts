import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import _ from 'lodash'
// Import pages

Vue.use(VueRouter)

export const Routes = {
  home: {
    path: '/',
    name: 'home',
    redirect: { name: 'comments' }
  },

  root: {
    path: '/',
    name: 'root',
    redirect: { name: 'comments' }
  },

  comments: {
    path: '/comments',
    name: 'comments',
    meta: { title: 'comments' }
  },

  events: {
    path: '/events',
    name: 'events',
    meta: { owner: true, title: 'eventsdays - My Events' }
  },

  profile: {
    path: '/profile',
    name: 'profile',
    meta: { owner: true, title: 'eventsdays - profile' }
  },

  meetups: {
    path: '/meetups',
    name: 'meetups',
    meta: { owner: true, title: 'eventsdays - My Meetups' }
  },

  subscriptions: {
    path: '/subscriptions',
    name: 'subscriptions',
    meta: { title: 'eventsdays - My Subs' }
  },

  account: {
    path: '/account',
    children: {
      index: {
        name: 'accountIndex',
        path: '/account'
      },
      details: {
        name: 'accountDetails',
        path: '/account/details'
      }
    }
  },

  user: {
    path: '/user',
    name: 'user',
    children: {
      index: {
        name: 'index',
        path: '/user'
      },
      details: {
        name: 'details',
        path: '/user/details'
      }
    }
  },

  login: {
    path: '/login',
    meta: { layout: 'plain', authPage: true },
    redirect: { name: 'loginMain' },
    children: {
      main: {
        name: 'loginIndex',
        path: '/login',
        meta: { layout: 'plain' } //todo: is needed?
      },
      signup: {
        name: 'loginSignup',
        path: '/login/signup'
      },
      signin: {
        name: 'loginSignin',
        path: '/login/signin'
      },
      forgot: {
        path: '/login/forgot'
      }
    }
  },

  myEvents: {
    name: 'myEvents',
    path: '/my-events',
    children: {
      index: {
        path: '/my-events'
      },
      past: {
        path: '/my-events/past'
      },
      organized: {
        path: '/my-events/organized'
      }
    }
  },

  map: {
    name: 'map',
    path: '/map'
  },

  logout: {
    name: 'logout',
    path: '/logout'
  },

  redirect: { path: '*', redirect: '/' }
}

const normalizeChildren = (r: Types.RoutesItem): RouteConfig => {
  if (!r.children) return r
  return { ...r, children: _.values(r.children) }
}

// Convert routes obj into an array of routes, compatible with VueRouter
const routes = _.values(Routes).map(normalizeChildren)

const router = new VueRouter({
  mode: 'history',
  scrollBehavior() {
    return { x: 0, y: 0 } // resetting scroll after route change
  },
  routes
})

export default router
